import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouteReuseStrategy, RouterModule, Routes} from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './component/app.component';
import { PlayerSignupComponent } from './component/player-signup/player-signup.component';
import {FormsModule} from "@angular/forms";
import {HomePage} from "./component/home/home.page";
import {HttpClientModule} from "@angular/common/http";
import { GameComponent } from './component/home/routable/game/game.component';
import { LeaderBoardComponent } from './component/home/routable/leader-board/leader-board.component';
import { MenuComponent } from './component/home/routable/menu/menu.component';
import {PlayerService} from "./service/player.service";
import {TriviaService} from "./service/trivia.service";

const routes: Routes = [
  {
    path: '',
    component: MenuComponent,
  },
  {
    path: 'play',
    component: GameComponent,
  },
  {
    path: 'leader-board',
    component: LeaderBoardComponent,
  },
];

@NgModule({
  declarations: [

    AppComponent,
    HomePage,
    PlayerSignupComponent,
    GameComponent,
    LeaderBoardComponent,
    MenuComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy,
    },
    PlayerService,
    TriviaService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
