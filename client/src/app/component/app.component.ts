import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Player} from "../../model/player";
import {LocalStorage} from "ngx-store";
import {PlayerService} from "../service/player.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  @LocalStorage()
  player: Player;

  constructor(
    private readonly platform: Platform,
    private readonly splashScreen: SplashScreen,
    private readonly statusBar: StatusBar,
    private readonly playerService: PlayerService
  ) {
    this.initializeApp();

    this.playerService.gameCompleted
                      .subscribe(
                        scoreTime => {
                          this.player = {
                            ...this.player,
                            ...scoreTime
                          };
                          this.playerService.create(this.player);
                        });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
