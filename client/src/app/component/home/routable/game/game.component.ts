import {Component, OnInit} from '@angular/core';
import {Trivias} from "../../../../../model/trivias";
import {TriviaService} from "../../../../service/trivia.service";
import {Difficulty, Trivia, Type} from "../../../../../model/trivia";
import {Error} from "tslint/lib/error";
import {PlayerService} from "../../../../service/player.service";
import {Router} from "@angular/router";
import {LocalStorage} from "ngx-store";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  difficulty: Difficulty;

  @LocalStorage()
  trivias: Trivias;

  current: Trivia;

  currentAnswers: string[];

  currentIndex: number;

  Difficulty: any = Difficulty;

  Type: any = Type;

  startTime: number;

  endTime: number;

  score: number = 0;

  constructor(
    private readonly triviaService: TriviaService,
    private readonly playerService: PlayerService,
    private readonly router: Router
  ) {
  }

  start(difficulty: Difficulty) {
    this.difficulty = difficulty;
    this.triviaService.get(20, difficulty)
                      .subscribe(trivias => {
                        this.trivias = trivias;
                        this.startTime = Date.now();
                        this.next();
                      });
  }

  next() {
    this.current = this.trivias.results.pop();

    if(!this.current)
      this.endTime = Date.now();
    else {
      this.currentAnswers = [
        this.current.correct_answer,
        ...this.current.incorrect_answers
      ].sort(
        (q0, q1) =>
          (Date.now() % 2 ? 1 : -1) * (q0.length - q1.length)
      );
      this.currentIndex = null;
    }
  }

  pick(index: number) {
    this.score +=
      (this.current.correct_answer == this.currentAnswers[index] ? +1 : -1) *
      GameComponent.getBasePoints(this.difficulty);
    this.currentIndex = index;
  }

  getColor(index: number): string {
    if(this.currentIndex != index)
      return '';
    return this.current.correct_answer == this.currentAnswers[index] ?
      'success' :
      'danger';
  }

  get time(): number {
    return this.endTime - this.startTime;
  }

  close() {
    this.playerService.gameCompleted.emit({
      score: this.score,
      time: this.time
    });

    this.router.navigateByUrl('/');

  }

  private static getBasePoints(difficulty: Difficulty): number {
    switch(difficulty) {
      case Difficulty.EASY:
        return 5;
      case Difficulty.MEDIUM:
        return 10;
      case Difficulty.HARD:
        return 20;
      default:
        throw new Error(difficulty);
    }
  }

  ngOnInit() {
  }
}
